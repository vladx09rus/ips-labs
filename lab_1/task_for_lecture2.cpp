#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <cilk/reducer_max.h>
#include <cilk/reducer_min.h>
#include <cilk/reducer_vector.h>
#include <chrono>
#include <vector>
#include <cstdlib>


using namespace std::chrono;

/// ������� ReducerMaxTest() ���������� ������������ ������� �������,
/// ����������� �� � �������� ���������, � ��� �������
/// mass_pointer - ��������� �������� ������ ����� �����
/// size - ���������� ��������� � �������
void ReducerMaxTest(int *mass_pointer, const long size)
{
	cilk::reducer<cilk::op_max_index<long, int>> maximum;
	cilk_for(long i = 0; i < size; ++i)
	{
		maximum->calc_max(i, mass_pointer[i]);
	}
	printf("Maximal element = %d has index = %d\n",
		maximum->get_reference(), maximum->get_index_reference());
}

void ReducerMinTest(int *mass_pointer, const long size)
{
	cilk::reducer<cilk::op_min_index<long, int>> minimum;
	cilk_for(long i = 0; i < size; ++i)
	{
		minimum->calc_min(i, mass_pointer[i]);
	}
	printf("Minimal element = %d has index = %d\n",
		minimum->get_reference(), minimum->get_index_reference());
}

/// ������� ParallelSort() ��������� ������ � ������� �����������
/// begin - ��������� �� ������ ������� ��������� �������
/// end - ��������� �� ��������� ������� ��������� �������
void ParallelSort(int *begin, int *end)
{
	if (begin != end) 
	{
		--end;
		int *middle = std::partition(begin, end, std::bind2nd(std::less<int>(), *end));
		std::swap(*end, *middle); 
		cilk_spawn ParallelSort(begin, middle);
		ParallelSort(++middle, ++end);
		cilk_sync;
	}	
}

void CompareForAndCilk_For(const long size)
{
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	
	std::vector<int> for_vec;
	for (int i = 0; i < size; ++i) 
	{
		for_vec.push_back(std::rand() % 20000 + 1);
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> duration_for = (t2 - t1);
	printf("For duration is: %f \n", duration_for);

	cilk::reducer<cilk::op_vector<int>>red_vec;
	t1 = high_resolution_clock::now();
	for (int i = 0; i < size; ++i)
	{
		red_vec->push_back(std::rand() % 20000 + 1);
	}
	t2 = high_resolution_clock::now();
	duration<double> duration_cilc = (t2 - t1);
	printf("Cilc duration is: %f \n", duration_cilc);
	
	printf("Duration diff: %f \n", duration_cilc - duration_for);

}

int main(int argc, char* argv[])
{
	srand((unsigned)time(0));

	// ������������� ���������� ���������� ������� = 4
	__cilkrts_set_param("nworkers", "4");

	long i;
	long mass_size = 10000;
	if (argc > 1) {
		mass_size = atoi(argv[1]);
	}

	int en_comparsion = 0;
	if (argc == 3)
	{
		en_comparsion = atoi(argv[2]);
	}
	
	int *mass_begin, *mass_end;
	int *mass = new int[mass_size]; 
	printf("Mass size: %d\n\n", mass_size);
	for(i = 0; i < mass_size; ++i)
	{
		mass[i] = (rand() % 25000) + 1;
	}
	
	mass_begin = mass;
	mass_end = mass_begin + mass_size;
	ReducerMaxTest(mass, mass_size);
	ReducerMinTest(mass, mass_size);

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	ParallelSort(mass_begin, mass_end);
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> duration = (t2 - t1);
	printf("\nSort duration is: %f \n\n", duration);

	ReducerMaxTest(mass, mass_size);
	ReducerMinTest(mass, mass_size);
	delete[]mass;

	printf("--------------------------------------\n");

	if (!en_comparsion)
	{
		return 0;
	}
	printf("\nComparsion:\n");
	long sizes[8] = { 1000000, 100000, 10000, 1000, 500, 100, 50, 10 };
	for (int i = 0; i < 8; ++i)
	{
		printf("Size: %d\n", sizes[i]);
		CompareForAndCilk_For(sizes[i]);
		printf("\n");
	}
	
	return 0;
}