#include <stdio.h>
#include <cilk/cilk.h>
#include <cilk/reducer_opadd.h>
#include <chrono>
#include <math.h>

using namespace std::chrono;

static long num_steps = 1000000000;

double left_range = 0;
double right_range = 1;


double fun(double x)
{
	return 4.0/(1+x*x);
}

void parallel(double dx)
{

	cilk::reducer<cilk::op_add<double>> sum(0);
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	cilk_for(int i = 0; i < num_steps; i++)
	{
		*sum += fun(i*dx)*dx;
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> duration = (t2 - t1);
	printf("Cilk for duration is: %f \n", duration);
	printf("Cilk for result: %2.20f\n\n", sum.get_value());
}

void serial(double dx)
{
	double sum2 = 0;
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	for (int i = 0; i < num_steps; i++)
	{
		sum2 += fun(i*dx)*dx;
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> duration = (t2 - t1);
	printf("Plain for duration is: %f \n", duration);
	printf("Plain for result: %2.20f\n", sum2);
}

int main()
{
	double dx = (right_range - left_range) / (double)num_steps;
	printf("Calculating integral ...\n");
	parallel(dx);
	serial(dx);
}